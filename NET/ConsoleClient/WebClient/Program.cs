﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace WebClient
{
    class Program
    {
        #region Constants
        const string USAGE =
@"Converts files from XML into JSON or from JSON into XML

Convert source [destination]
source      Specifies the file to be converted
destination Specifies destination's file name (optional). If not destination file name specified the name and location of the source file will be used

Format of source file will be automatically detected";

        #region Messages
        const string ERR_MSG_FILE_DOESNOT_EXIST = @"Specified file [{0}] does not exist";
        const string ERR_MSG_FILETYPE_UNKNOWN = @"Cannot detect source file format";
        const string MSG_SUCCESS = "The [{0}] file was successfully created";
        #endregion Messages

        const int ERR_CODE_PROCESSING_FAILED = 290;

        #endregion Constants

        /// <summary>
        /// Supported file types
        /// </summary>
        enum FILE_TYPE : int
        {
            UNKNOWN,
            XML,
            JSON,
        }

        static Uri serviceUri = new Uri("http://localhost:58986/api/converter");

        static void Main(string[] args)
        {
            #region Input Validation

            if (args.Length < 1 || args.Length > 2)
            {
                Console.WriteLine(USAGE);
                return;
            }

            String sourceFileName = args[0];
            String destinationFileName = default(String);

            if (!File.Exists(sourceFileName))
            {
                Console.WriteLine(String.Format(ERR_MSG_FILE_DOESNOT_EXIST, sourceFileName));
                return;
            }

            bool isDestFileSpecified = args.Length == 2;
            // if only source file was provided:
            if (isDestFileSpecified)
                destinationFileName = args[1];
            else
            {
                string ext = Path.GetExtension(sourceFileName);
                destinationFileName = sourceFileName;
                if (ext.Length > 0)
                    destinationFileName = Path.Combine(Path.GetDirectoryName(sourceFileName)
                        , Path.GetFileNameWithoutExtension(sourceFileName));

                destinationFileName += ".unknwn";
            }

            #endregion Input Validation

            List<string> fileValidationErrors = new List<string>();
            FILE_TYPE ftype = FILE_TYPE.UNKNOWN;
            // try to create destination file name
            try
            {
                using (FileStream destinationFile = File.Create(destinationFileName))
                using (FileStream sourceFile = File.OpenRead(sourceFileName))
                {
                    // create a web request
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(serviceUri);
                    request.KeepAlive = false;
                    request.ProtocolVersion = HttpVersion.Version11;
                    request.Method = "POST";
                    request.ContentType = "text/plain";
                    int timeout = 500;
                    if (timeout < 0)
                    {
                        request.ReadWriteTimeout = timeout;
                        request.Timeout = timeout;
                    }
                    try
                    {
                        // server will try to determing type of the source file
                        submitForProcessing(request, sourceFile, destinationFile, out ftype);
                    }
                    catch
                    {
                        request.Abort();
                        throw;
                    }
                }

                // if destination file was not provided by user - rename the "temp" file according to its content
                if (isDestFileSpecified == false)
                {
                    // create new unique file name
                    string newDestFileName = Path.Combine(
                                                    Path.GetDirectoryName(sourceFileName),
                                                    String.Format("{0}_{1}_out.{2}"
                                                            , Path.GetFileNameWithoutExtension(destinationFileName)
                                                            , DateTime.Now.ToString("yyyyMMddHHmmss")
                                                            , ftype.ToString()));
                    File.Move(destinationFileName, newDestFileName);
                    destinationFileName = newDestFileName;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                // If content is not XML nor JSON - display error message and exit
                if (ftype == FILE_TYPE.UNKNOWN)
                {
                    Console.WriteLine(ERR_MSG_FILETYPE_UNKNOWN);
                    if (File.Exists(destinationFileName))
                        File.Delete(destinationFileName);
                }
                else
                    Console.WriteLine(String.Format(MSG_SUCCESS, destinationFileName));
            }
        }

        /// <summary>
        /// Sends data for processing to a RESTful service
        /// </summary>
        /// <param name="sIn">Incoming data</param>
        /// <param name="sOut">Outcoming results</param>
        /// <param name="ftype">Type of sending data (XML or JSON)</param>
        static void submitForProcessing(HttpWebRequest request, Stream sIn, Stream sOut, out FILE_TYPE ftype)
        {
            ftype = FILE_TYPE.UNKNOWN;

            StreamReader srIn = new StreamReader(sIn);
            
            byte[] postBytes = Encoding.UTF8.GetBytes(srIn.ReadToEnd());
            request.ContentLength = postBytes.Length;

            using (Stream requestStream = request.GetRequestStream())
            {
                requestStream.Write(postBytes, 0, postBytes.Length);
                requestStream.Flush();

                using (var response = (HttpWebResponse)request.GetResponse())
                using (Stream rs = response.GetResponseStream())
                {
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        StreamReader sr = new StreamReader(rs);
                        byte[] resposeBytes = Encoding.UTF8.GetBytes(sr.ReadToEnd());
                        sOut.Write(resposeBytes, 0, resposeBytes.Length);
                        string responseContentType = response.ContentType;
                        if (!String.IsNullOrEmpty(responseContentType))
                        {
                            responseContentType = responseContentType.ToLower();
                            if (responseContentType.Contains("xml"))
                                ftype = FILE_TYPE.XML;
                            else if (responseContentType.Contains("json"))
                                ftype = FILE_TYPE.JSON;
                        }
                    }
                    // since we know that what error code will be send back to use in case of FAILURE:
                    else if ((int)response.StatusCode == ERR_CODE_PROCESSING_FAILED)
                    {
                        string serverError = String.Empty;
                        try
                        {
                            if (rs.CanRead)
                            {
                                StreamReader sr = new StreamReader(rs);
                                serverError = sr.ReadToEnd();
                            }
                        }
                        catch
                        {
                            serverError = response.StatusDescription;
                        }
                        throw new Exception(serverError);
                    }
                }
            }
        }
    }
}
