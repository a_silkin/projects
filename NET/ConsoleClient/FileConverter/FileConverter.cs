﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Linq;

namespace FileConverter
{
    /// <summary>
    /// Helper class to convert files JSON <-> XML
    /// </summary>
    public class XmlJsonConverter
    {
        /// <summary>
        /// Supported file types
        /// </summary>
        public enum FILE_TYPE : int
        {
            UNKNOWN,
            XML,
            JSON,
        }

        const string ERR_MSG_TYPE_UNKNOWN = @"Cannot detect source file format";
        const string ERR_MSG_ARGUMENT_NULL = @"Argument(s) cannot be NULL";

        /// <summary>
        /// Converts incoming XML stream into JSON
        /// The method will throw parsing exceptions which need to be handeled in consumer's code
        /// </summary>
        /// <param name="xmlIn">Input XML as Stream</param>
        /// <param name="jsonOut">Output JSON as Stream</param>
        public static void Xml2Json(Stream xmlIn, Stream jsonOut)
        {
            if (xmlIn == null || jsonOut == null)
                throw new ArgumentNullException(ERR_MSG_ARGUMENT_NULL);

            XmlReaderSettings settings = new XmlReaderSettings()
            {
                DtdProcessing = DtdProcessing.Ignore,
                IgnoreWhitespace = true,
                IgnoreProcessingInstructions = true
            };

            using (XmlReader xmlReader = XmlReader.Create(xmlIn, settings))
            {
                XDocument xdoc = XDocument.Load(xmlReader);
                var prevPosition = jsonOut.Position;
                StreamWriter sw = new StreamWriter(jsonOut);
                sw.Write(JsonConvert.SerializeXNode(xdoc, Newtonsoft.Json.Formatting.Indented));
                sw.Flush();
                jsonOut.Position = prevPosition;
            }
        }

        /// <summary>
        /// Converts incoming JSON stream into XML
        /// The method will throw parsing exceptions which need to be handeled in consumer's code
        /// </summary>
        /// <param name="jsonIn">Output JSON as Stream</param>
        /// <param name="xmlOut">Input XML as Stream</param>
        public static void Json2Xml(Stream jsonIn, Stream xmlOut)
        {
            if (jsonIn == null || xmlOut == null)
                throw new ArgumentNullException(ERR_MSG_ARGUMENT_NULL);

            StreamReader sr = new StreamReader(jsonIn);
            string json = sr.ReadToEnd();
            XNode node = JsonConvert.DeserializeXNode(json);
            var prevPosition = xmlOut.Position;
            StreamWriter sw = new StreamWriter(xmlOut);
            sw.Write(node.ToString());
            sw.Flush();
            xmlOut.Position = prevPosition;
        }
        
        /// <summary>
        /// Helper method to check a type of incoming stream
        /// </summary>
        /// <param name="sourceStream">incoming XML or JSON stream</param>
        /// <param name="validationErrors">Collection of parsing errors if was not possible to validate </param>
        /// <returns>Type of incoming stream as FILE_TYPE. XML, JSON or UNKNOWN</returns>
        public static string DetectInputStreamType(Stream sourceStream, out List<string> validationErrors)
        {
            FILE_TYPE ftype = FILE_TYPE.UNKNOWN;
            validationErrors = new List<string>();

            if (sourceStream == null || sourceStream.Length == 0)
                return ftype.ToString();

            long originalStreamPosition = sourceStream.Position;
            StreamReader sr = new StreamReader(sourceStream);
            StringBuilder buffer = new StringBuilder();
            buffer.Append(sr.ReadToEnd());
            #region Source file format automatic detection
            // 1. detect source file type
            try
            {
                // is the file in JSON format:
                JsonTextReader jr = new JsonTextReader(new StringReader(buffer.ToString()));
                while (jr.Read()) ;
                ftype = FILE_TYPE.JSON;
            }
            catch (Exception ex)
            {
                validationErrors.Add(ex.Message);
                ftype = FILE_TYPE.UNKNOWN;
            }
            // next try XML parser
            if (ftype == FILE_TYPE.UNKNOWN)
            {
                try
                {
                    // is the file in XML format?
                    XDocument xdoc = XDocument.Load(new StringReader(buffer.ToString()), LoadOptions.None);
                    ftype = FILE_TYPE.XML;
                }
                catch (Exception ex)
                {
                    validationErrors.Add(ex.Message);
                    ftype = FILE_TYPE.UNKNOWN;
                }
            }

            // could not idetify file type: Output discovered errors and exit
            if (ftype == FILE_TYPE.UNKNOWN)
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine(ERR_MSG_TYPE_UNKNOWN);
                foreach (string err_msg in validationErrors)
                    sb.AppendLine(err_msg);
            }
            else
                validationErrors.Clear();

            sourceStream.Position = originalStreamPosition;

            #endregion Source file format detection
            return ftype.ToString();
        }
    }
}
