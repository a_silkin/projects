﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using FileConverter;

namespace ConsoleApp
{
    class Program
    {
        #region Constants
        const string USAGE =
@"Converts files from XML into JSON or from JSON into XML

Convert source [destination]
source      Specifies the file to be converted
destination Specifies destination's file name (optional). If not destination file name specified the name and location of the source file will be used

Format of source file will be automatically detected";

        #region Messages
        const string ERR_MSG_FILE_DOESNOT_EXIST = @"Specified file [{0}] does not exist";
        const string ERR_MSG_TYPE_UNKNOWN = @"Cannot detect source file format";
        const string MSG_SUCCESS = "The [{0}] file was successfully created";
        #endregion Messages

        #endregion Constants

        static void Main(string[] args)
        {
            #region Input Validation

            if (args.Length < 1 || args.Length > 2)
            {
                Console.WriteLine(USAGE);
                return;
            }

            String sourceFileName = args[0];
            String destinationFileName = default(String);

            if (!File.Exists(sourceFileName))
            {
                Console.WriteLine(String.Format(ERR_MSG_FILE_DOESNOT_EXIST, sourceFileName));
                return;
            }

            bool isDestFileSpecified = args.Length == 2;
            // if only source file was provided:
            if (isDestFileSpecified)
                destinationFileName = args[1];
            else
            {
                string ext = Path.GetExtension(sourceFileName);
                destinationFileName = sourceFileName;
                if (ext.Length > 0)
                    destinationFileName = Path.Combine(Path.GetDirectoryName(sourceFileName)
                        , Path.GetFileNameWithoutExtension(sourceFileName));

                destinationFileName += ".unknwn";
            }
            #endregion Input Validation

            // try to create destination file name
            XmlJsonConverter.FILE_TYPE ftype = XmlJsonConverter.FILE_TYPE.UNKNOWN;
            try
            {
                using (FileStream destinationFile = File.Create(destinationFileName))
                using (FileStream sourceFile = File.OpenRead(sourceFileName))
                {
                    convertStreams(sourceFile, destinationFile, out ftype);
                }

                // if destination file was not provided by user - rename the "temp" file according to its content
                if (isDestFileSpecified == false)
                {
                    // create new unique file name
                    string newDestFileName = Path.Combine(
                                                    Path.GetDirectoryName(sourceFileName), 
                                                    String.Format("{0}_{1}_out.{2}"
                                                            , Path.GetFileNameWithoutExtension(destinationFileName)
                                                            , DateTime.Now.ToString("yyyyMMddHHmmss")
                                                            , ftype.ToString()));
                    File.Move(destinationFileName, newDestFileName);
                    destinationFileName = newDestFileName;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                // If content is not XML nor JSON - display error message and exit
                if (ftype == XmlJsonConverter.FILE_TYPE.UNKNOWN)
                {
                    Console.WriteLine(ERR_MSG_TYPE_UNKNOWN);
                    if (File.Exists(destinationFileName))
                        File.Delete(destinationFileName);
                }
                else
                    Console.WriteLine(String.Format(MSG_SUCCESS, destinationFileName));
            }
        }

        /// <summary>
        /// Sends data for processing to a RESTful service
        /// </summary>
        /// <param name="sIn">Incoming data</param>
        /// <param name="sOut">Outcoming results</param>
        /// <param name="ftype">Type of sending data (XML or JSON)</param>
        static void convertStreams(Stream sIn, Stream sOut, out XmlJsonConverter.FILE_TYPE ftype)
        {
            ftype = XmlJsonConverter.FILE_TYPE.UNKNOWN;
            long startingPosition = sIn.Position;
            try
            {
                XmlJsonConverter.Xml2Json(sIn, sOut);
                ftype = XmlJsonConverter.FILE_TYPE.XML;
            }
            catch
            {
                try
                {
                    sIn.Position = startingPosition;
                    XmlJsonConverter.Json2Xml(sIn, sOut);
                    ftype = XmlJsonConverter.FILE_TYPE.JSON;
                }
                catch
                {
                }
            }
        }
    }
}
