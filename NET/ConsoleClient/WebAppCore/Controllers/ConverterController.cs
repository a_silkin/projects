﻿using System;
using System.IO;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using FileConverter;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Xml.Linq;
using System.Text;
using static FileConverter.XmlJsonConverter;

namespace WebAppCore.Controllers
{
    [Route("api/[controller]")]
    public class ConverterController : Controller
    {
        /// <summary>
        /// Converts incoming stream of XML or JSON data into JSON or XML 
        /// </summary>
        /// <returns>New stream XML or JSON data</returns>
        [HttpPost]
        public string Post()
        {
            XmlJsonConverter.FILE_TYPE ftype = XmlJsonConverter.FILE_TYPE.UNKNOWN;
            List<string> validationErrors = new List<string>();
            using (StreamReader sr = new StreamReader(Request.Body))
            // cache incoming data
            using (MemoryStream inputStream = new MemoryStream())
            {
                byte[] buffer = Encoding.UTF8.GetBytes(sr.ReadToEnd());
                inputStream.Write(buffer, 0, buffer.Length);
                inputStream.Position = 0;
                try
                {
                    inputStream.Position = 0;
                    using (MemoryStream outputStream = new MemoryStream())
                    {
                        // is the data in XML?
                        try
                        {
                            XmlJsonConverter.Xml2Json(inputStream, outputStream);
                            // resulting type will be JSON here
                            ftype = XmlJsonConverter.FILE_TYPE.JSON;
                        }
                        catch
                        {
                            // Is the data in JSON?
                            try
                            {
                                inputStream.Position = 0;
                                XmlJsonConverter.Json2Xml(inputStream, outputStream);
                                // resulting output will be XML than
                                ftype = XmlJsonConverter.FILE_TYPE.XML;
                            }
                            catch
                            {
                            }
                        }

                        if (ftype != FILE_TYPE.UNKNOWN)
                        {
                            Response.ContentType = ftype.ToString();
                            using (StreamReader outputReader = new StreamReader(outputStream))
                                return outputReader.ReadToEnd();
                        }
                        else
                        {
                            Response.StatusCode = ERR_CODE_PROCESSING_FAILED;
                            return ERR_MSG_TYPE_UNKNOWN;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Response.StatusCode = ERR_CODE_PROCESSING_FAILED;
                    return ex.Message;
                }
            }
        }

        const string ERR_MSG_TYPE_UNKNOWN = @"Cannot detect source file format.";
        const int ERR_CODE_PROCESSING_FAILED = 290;
    }
}
