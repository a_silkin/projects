using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using System.Collections.Generic;

namespace UnitTestProject
{
    [TestClass]
    public class UnitTests
    {
#region XMl and JSON strings
        static string xmlIn =
@"<?xml version='1.0' encoding='utf-8'?>
<!--Copyright, Microsoft Corporation, All rights reserved.-->
<Rule Name='WebServiceDebugger'
      DisplayName='Web Service Debugger'
      PageTemplate='debugger'
      Description='Web Service Debugger options'
      xmlns='http://schemas.microsoft.com/build/2009/properties'>
  <Rule.DataSource>
    <DataSource Persistence='UserFile' />
  </Rule.DataSource>
  <StringProperty Name='WebServiceDebuggerHttpUrl' DisplayName='HTTP URL'
                Description='Specifies the URL for the project.'
                F1Keyword='VC.Project.IVCWebSvcDebugPageObject.HttpUrl'/>
  <EnumProperty Name='WebServiceDebuggerDebuggerType' DisplayName='Debugger Type'
                Description='Specifies the debugger type to use. When set to Auto, the debugger type will be selected based on contents of the exe file.'
                F1Keyword='VC.Project.IVCWebSvcDebugPageObject.DebuggerType'>
    <EnumValue Name='NativeOnly' DisplayName='Native Only' Description='Native Only' />
    <EnumValue Name='ManagedOnly' DisplayName='Managed Only' Description='Managed Only' />
    <EnumValue Name='Mixed' DisplayName='Mixed' Description='Mixed' />
    <EnumValue Name='Auto' DisplayName='Auto' Description='Auto' />
    <EnumValue Name='Script' DisplayName='Script' Description='Script' />
  </EnumProperty>
  <BoolProperty Name='WebServiceDebuggerSQLDebugging' DisplayName='SQL Debugging'
                Description='Attach the SQL debugger.'
                F1Keyword='VC.Project.IVCWebSvcDebugPageObject.SQLDebugging'/>
  <BoolProperty Name='UseLegacyManagedDebugger' Visible='false' />
</Rule>";

        static string jsonIn =
@"{
  '?xml': {
    '@version': '1.0',
    '@encoding': 'utf-8'
  }/*Copyright, Microsoft Corporation, All rights reserved.*/,
  'Rule': {
    '@Name': 'WebServiceDebugger',
    '@DisplayName': 'Web Service Debugger',
    '@PageTemplate': 'debugger',
    '@Description': 'Web Service Debugger options',
    '@xmlns': 'http://schemas.microsoft.com/build/2009/properties',
    'Rule.DataSource': {
      'DataSource': {
        '@Persistence': 'UserFile'
      }
    },
    'StringProperty': {
      '@Name': 'WebServiceDebuggerHttpUrl',
      '@DisplayName': 'HTTP URL',
      '@Description': 'Specifies the URL for the project.',
      '@F1Keyword': 'VC.Project.IVCWebSvcDebugPageObject.HttpUrl'
    },
    'EnumProperty': {
      '@Name': 'WebServiceDebuggerDebuggerType',
      '@DisplayName': 'Debugger Type',
      '@Description': 'Specifies the debugger type to use. When set to Auto, the debugger type will be selected based on contents of the exe file.',
      '@F1Keyword': 'VC.Project.IVCWebSvcDebugPageObject.DebuggerType',
      'EnumValue': [
        {
          '@Name': 'NativeOnly',
          '@DisplayName': 'Native Only',
          '@Description': 'Native Only'
        },
        {
          '@Name': 'ManagedOnly',
          '@DisplayName': 'Managed Only',
          '@Description': 'Managed Only'
        },
        {
          '@Name': 'Mixed',
          '@DisplayName': 'Mixed',
          '@Description': 'Mixed'
        },
        {
          '@Name': 'Auto',
          '@DisplayName': 'Auto',
          '@Description': 'Auto'
        },
        {
          '@Name': 'Script',
          '@DisplayName': 'Script',
          '@Description': 'Script'
        }
      ]
    },
    'BoolProperty': [
      {
        '@Name': 'WebServiceDebuggerSQLDebugging',
        '@DisplayName': 'SQL Debugging',
        '@Description': 'Attach the SQL debugger.',
        '@F1Keyword': 'VC.Project.IVCWebSvcDebugPageObject.SQLDebugging'
      },
      {
        '@Name': 'UseLegacyManagedDebugger',
        '@Visible': 'false'
      }
    ]
  }
}";

        #endregion XML and JSON strings

        [TestMethod]
        public void Xml2JsonTest()
        {

            using (var streamIn = new MemoryStream())
            using (var streamOut = new MemoryStream())
            {
                var sw = new StreamWriter(streamIn);
                sw.Write(xmlIn);
                sw.Flush();
                streamIn.Position = 0;
                try
                {
                    FileConverter.XmlJsonConverter.Xml2Json(streamIn, streamOut);
                    StreamReader sr = new StreamReader(streamOut);
                    var json = sr.ReadToEnd();
                    streamOut.Position = 0;
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex.Message);
                    throw;
                }
            }
        }

        [TestMethod]
        public void Json2XmlTest()
        {
            using (var streamIn = new MemoryStream())
            using (var streamOut = new MemoryStream())
            {
                var sw = new StreamWriter(streamIn);
                sw.Write(jsonIn);
                sw.Flush();
                streamIn.Position = 0;
                try
                {
                    FileConverter.XmlJsonConverter.Json2Xml(streamIn, streamOut);
                    StreamReader sr = new StreamReader(streamOut);
                    var xmlOut = sr.ReadToEnd();
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex.Message);
                    throw;
                }
            }
        }

        [TestMethod]
        public void DetectIncomingStreamType()
        {
            using (var streamIn = new MemoryStream())
            {
                var sw = new StreamWriter(streamIn);
                sw.Write(jsonIn);
                sw.Flush();
                streamIn.Position = 0;
                try
                {
                    string res = FileConverter.XmlJsonConverter.DetectInputStreamType(streamIn, out List<string> errors);
                    if (res != "JSON" || errors.Count > 0)
                        throw new Exception("Test failed!");
                        
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex.Message);
                    throw;
                }
            }
        }

    }
}
