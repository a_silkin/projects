The solution relies on the Newtonsoft.JSON package that implements low level conversions between XML <--> JSON  formats.
RESTful API is implemented in WebConverterService project. It has the only implemented method POST() that reads data from 
Request’s body and tries to convert the stream into XML or JSON if conversion to XML failed. 
If both conversion attempts are failed the service returns error message.

The solution contains following projects:

1. FileConverters.csproj - Library of file converters (implements transformation of XML stream to JSON stream or JSON stream into XML stream)
	The project has dependency on Newtonsoft.JSON package (v: 9.0.1) Please use NuGet to restore required dependencies

2. WebConverterService.csproj - RESTul service (http://localhost:58986/api/converter). 
	The service accepts input stream of JSON or XML intput and converts into output stream of XML or JSON.
	It depends on FileConverters.csproj

3. WebClient.csproj - Webclient to submit a file from command line to the WebConverterService. 
	The client simply sends data from provided source file and reports results back to a user

4. ConsoleApp.proj is .NETCore project that provides local conversion between XML <--> JSON formats.
	It depends on FileConverters.csproj

5. UnitTestsProject.csproj - tests for FileConverters library

How to run

HTTP API:
1. Start WebConvertersService. The service is preset to run on http://localhost:58986/api/converter
2. run the WebClient.exe with a file to process in command line: ConsoleClient.exe sourceFile destination (optional)
	if destination file is not specified an output file will be created as {sourceFile}_{timestamp}.{xml or json}

Standalone client:
1. Run ConsoleApp :
dotnet.exe ConsoleApp.dll sourceFile destination (optional)

	if destination file is not specified an output file will be created as {sourceFile}_{timestamp}.{xml or json}